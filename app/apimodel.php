<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class apimodel extends Model
{
    protected $table = 'insert';
    protected $fillable =['firstname','lastname','email','contact'];
}
