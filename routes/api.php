<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::post('/insert','insert@create');


Route::get('/crud', 'apicontroller@create');
Route::post('/crud', 'apicontroller@create');
Route::delete('/delete/{id}','apicontroller@deletebyid');
Route::put('/update/{id}', 'apicontroller@updatebyid');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*Route::get('/test', function(){
    
    $test = [
        'first_name'=> 'Ruchika',
        'Last_name'=> 'Shrotriya',
    ];
    return $test;
});*/




